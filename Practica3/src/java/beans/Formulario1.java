/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author MI PC
 */
public class Formulario1 {
    private String nombre; 
    private String apellido;
    
    public Formulario1(String nombre, String apellido){
        this.nombre=nombre;
        this.apellido=apellido;
    }

    public Formulario1() {
        this.nombre="";
        this.apellido="";
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    
    public String nombreEnviado(){
        String nombreCompleto = "El nombre enviado fue:" + getNombre()+" "+getApellido(); 
        return nombreCompleto;
    }
}
