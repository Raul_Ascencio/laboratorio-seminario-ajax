/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author MI PC
 */
public class Formulario2 {
    private String nombre; 
    private String pais;
    
    public Formulario2(String nombre, String pais){
        this.nombre=nombre;
        this.pais=pais;
    }
    public Formulario2(){
        this.nombre ="";
        this.pais="";
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    
    public String personaYPais(){
        String dato = "El usuario:"+getNombre()+" viene de "+getPais();
        return dato;
    }
}
