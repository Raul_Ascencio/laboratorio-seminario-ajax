/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.Formulario1;
import beans.Formulario2;

/**
 *
 * @author MI PC
 */
@WebServlet(name = "FormularioServlet", urlPatterns = {"/FormularioServlet"})
public class FormularioServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Formulario2 formulario = new Formulario2(request.getParameter("nombre"), request.getParameter("pais"));
        procesaFormulario2(request, response, formulario);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Formulario1 formulario = new Formulario1(request.getParameter("nombre"), request.getParameter("apellido"));
        procesaFormulario1(request, response, formulario);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void procesaFormulario1(HttpServletRequest request, HttpServletResponse response, Formulario1 formulario)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setAttribute("formulario1", formulario);
        RequestDispatcher a = request.getRequestDispatcher("./jsp/resultados.jsp");
        a.forward(request, response);
       
    }

    private void procesaFormulario2(HttpServletRequest request, HttpServletResponse response, Formulario2 formulario) 
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        request.setAttribute("formulario2", formulario);
        RequestDispatcher a = request.getRequestDispatcher("./jsp/resultados.jsp");
        a.forward(request, response);
    }

}
