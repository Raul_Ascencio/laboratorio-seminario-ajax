<%-- 
    Document   : formularios
    Created on : 18-oct-2020, 18:41:26
    Author     : MI PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formularios</title>
    </head>
    <body>
        <h1>Por favor llene alguno de los  siguientes formularios</h1>
        <p>Formulario 1(Con POST)</p>
        <form action="/Practica3/FormularioServlet" method ="post">
            <table>
                <tr>
                    <td>Nombre</td> <td><input type ="text" placeholder="Nombre" name="nombre"/></td>
                </tr>
                <tr>
                    <td>Apellido</td> <td><input type ="text" placeholder="Apellido" name="apellido"/></td>
                </tr>
                <tr>
                    <td><button type="submit">Enviar</button></td>
                </tr>
            </table>
        </form>
        <p>Formulario 2 (Con GET)</p>
        <form action="/Practica3/FormularioServlet" method="get">
            <table>
                <tr>
                    <td>Nombre</td> <td><input type ="text" placeholder="Nombre" name="nombre"/></td>
                </tr>
                <tr>
                    <td>Apellido</td> <td><input type ="text" placeholder="Pais" name="pais"/></td>
                </tr>
                <tr>
                    <td><button type="submit">Enviar</button></td>
                </tr>
            </table>
        </form>
    </body>
</html>
