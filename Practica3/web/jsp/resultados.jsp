<%-- 
    Document   : resultados
    Created on : 18-oct-2020, 19:35:09
    Author     : MI PC
--%>

<%@page import="java.util.Enumeration"%>
<%@page import="beans.Formulario2"%>
<%@page import="beans.Formulario1"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado formulario</title>
    </head>
    <body>
        <%
            Formulario1 formulario1 = new Formulario1();
            Formulario2 formulario2 = new Formulario2();
            boolean b = false; 
            Enumeration<String> enu = request.getAttributeNames();
            while (enu.hasMoreElements()){
                String element= enu.nextElement();
                if ("formulario2".equals(element)){
                    b = true;
                    break;
                } 
                if("formulario1".equals(element)){
                    break;
                }
            }
            if (b){
                formulario2 = (Formulario2) request.getAttribute("formulario2");
                if(formulario2 != null){
                    out.println(formulario2.personaYPais());
                }
            }else{
                formulario1 = (Formulario1) request.getAttribute("formulario1");
                if(formulario1 != null){
                    out.println(formulario1.nombreEnviado());
                }
                    
            }

        %>
    </body>
</html>
