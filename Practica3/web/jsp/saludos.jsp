<%-- 
    Document   : javasp
    Created on : 18-oct-2020, 18:00:11
    Author     : MI PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import='java.util.Date' %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/estilo1.css" rel="stylesheet" type="text/css">
        <title>Practica 3</title>
    </head>
    <body>
    <h1 class="textocs"> ¡Hola! Esta es mi página con jsp </h1>
    <%
        java.util.Calendar tiempo = java.util.Calendar.getInstance();
        int hora = tiempo.get(java.util.Calendar.HOUR_OF_DAY);
        int minuto = tiempo.get(java.util.Calendar.MINUTE);
        String saludo = "";
        if (6<= hora & hora <= 12){
            saludo += "Buenos días";
        }
        else if (hora <= 18){
            saludo += "Buenas tardes";
        }
        else if (saludo == ""){
            saludo += "Buenas noches";
        }        
    %>
    
    <p class="textocs"><%= saludo%>, la hora actual es:  <%= hora %>:<%= minuto%> </p>
    </body>
</html>
