/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raúl Ascencio
 */
public class ConexionBD {
    private String url = "jdbc:mysql://localhost:3306/mydb";
    
    //Constructor de la clase.
    public ConexionBD(){
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    //Método para verificar inicio de sesión.
    public Connection inciarConexion(){
        Connection conexion = null;
        try{
          conexion = DriverManager.getConnection(this.url, "root", "");
        } catch(SQLException e){
            System.err.println(e);
        } 
        return conexion;
    }
    
    public void cierraConexion(Connection conexion){
        try {
            conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
