<%-- 
    Document   : bienvenida
    Created on : 08-nov-2020, 22:45:52
    Author     : MI PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/estilos.css" rel="stylesheet" type="text/css">
        <title>Bienvenido</title>
    </head>
    <body>
        <h1>¡Bienvenido!</h1>
        <%
            String respuesta = (String) request.getAttribute("respuesta");
                out.println(respuesta);
        %>
    </body>
</html>
