/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author Raúl Ascencio Bolio
 */
public class PeticionesBD {
    
    /**
     *Método que regresa el usuario que inició sesión.
     * @param user     Nombre del usuario
     * @param password Contraseña del usuario
     * @return
     */
    public String obtenUsuario(String user, String password){
        String resultado = "Error usuario no valido";
        try {
            ConexionBD conexionbd= new ConexionBD();
            Connection conexion = conexionbd.inciarConexion();
            String query = "SELECT * FROM users WHERE user= '"+user+"' AND password= '"+password+"'";
            Statement st = conexion.prepareStatement(query);
            ResultSet rs = st.executeQuery(query);
            if(rs.next()){
                resultado = "Bienvenido "+rs.getString(1);
            }            
        }  catch(SQLException e){
            System.err.println(e);
        } 
        return resultado;
    }
    
    public String obtenImagen(int id){
        String nombre = "";
        try {
            ConexionBD conexionbd= new ConexionBD();
            Connection conexion = conexionbd.inciarConexion();
            String query = "SELECT * FROM imagenes WHERE id= "+id;
            Statement st = conexion.prepareStatement(query);
            ResultSet rs = st.executeQuery(query);
            if(rs.next()){
                nombre = rs.getString(2);
            }            
        }  catch(SQLException e){
            System.err.println(e);
        } 
        return nombre;
    }
    
    
}
