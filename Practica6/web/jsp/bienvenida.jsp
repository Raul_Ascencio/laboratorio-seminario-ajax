<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Practica 6</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/estilos.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" language="javascript">
            var xhr;
            
            function creaXMLHttp(){
                if ( window.ActiveXObject ){
                    xhr = new ActiveXObject( "Microsoft.XMLHttp" );      
                }
                else 
                    if( ( window.XMLHttpRequest ) || ( typeof XMLHttpRequest ) !== undefined ){
                        xhr = new XMLHttpRequest();
                    }
                else {
                    alert( "Su navegador no tiene soporte para AJAX" );
                    return;
                }
            }
            
            function cargaImagen(id){
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        var img = "<img class=\"imagenes\" src=\"./Imagenes/"+xhr.responseText+"\" alt=\"Gatito uwu\">";
                        document.getElementById("contenedor").innerHTML = img;
                    }
                };
                xhr.open("GET", "ManejadorImagenes?id="+id);
                xhr.send();
            }
            
            function cargaTexto(){
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        document.getElementById("contenedor").innerHTML = xhr.responseText;
                    }
                };
                xhr.open("GET", "textos/texto1.txt", true);
                xhr.send();
            }
            
            function cargaXml(){
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        var grupo = this.responseXML;
                        var informacion = "Las empresas que componen al Grupo Financiero Alfa son:\n\
                                           <ul>";                        
                        var empresas = grupo.getElementsByTagName("Empresa");
                        for(var i = 0; i < empresas.length; i++){
                            informacion += "<li>"+ empresas[i].getAttribute("nombre") +"</li>";
                        }
                        informacion += "</ul>";
                        document.getElementById("contenedor").innerHTML = informacion;
                    }
                };
                xhr.open("GET", "./xml/financieroAlfa.xml");
                xhr.send();
            }
        </script>
    </head>
    <body onload="creaXMLHttp()">
        <h1>Práctica 6</h1>
        <div id="imagen">
            <button type="button" class="btn btn-dark" onclick="cargaImagen('1')">Cargar Imagen</button>
        </div>
        <div id="texto">
            <button type="button" class="btn btn-dark" onclick="cargaTexto()">Cargar Texto</button>
        </div>
        <div id="xml">
            <button type="button" class="btn btn-dark" onclick="cargaXml()">Cargar Xml</button>
        </div>
        
        <div id="contenedor"></div>
    </body>
</html>

