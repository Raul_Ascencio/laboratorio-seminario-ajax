<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/estilos.css">
        <title>¡Error!</title>
    </head>
    <body>
        <h1>Error al iniciar sesión</h1>
        <a href="./index.html">Porfavor vuelva a intentarlo</a>
    </body>
</html>
